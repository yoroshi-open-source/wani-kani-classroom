<?php

/*
 * ----------------------------------------------------------------------------
 * # Custom Application Handler
 *
 * You can do a lot of things in this file.
 *
 * ## Set a theme by route:
 *
 * Route::setThemeByRoute('/login', 'greek_yogurt');
 *
 *
 * ## Register a class override.
 *
 * Core::bind('helper/feed', function() {
 * 	 return new \Application\Core\CustomFeedHelper();
 * });
 *
 * Core::bind('\Concrete\Attribute\Boolean\Controller', function($app, $params) {
 * 	return new \Application\Attribute\Boolean\Controller($params[0]);
 * });
 *
 * ## Register Events.
 *
 * Events::addListener('on_page_view', function($event) {
 * 	$page = $event->getPageObject();
 * });
 *
 *
 * ## Register some custom MVC Routes
 *
 * Route::register('/test', function() {
 * 	print 'This is a contrived example.';
 * });
 *
 * Route::register('/custom/view', '\My\Custom\Controller::view');
 * Route::register('/custom/add', '\My\Custom\Controller::add');
 *
 * ## Pass some route parameters
 *
 * Route::register('/test/{foo}/{bar}', function($foo, $bar) {
 *  print 'Here is foo: ' . $foo . ' and bar: ' . $bar;
 * });
 *
 *
 * ## Override an Asset
 *
 * use \Concrete\Core\Asset\AssetList;
 * AssetList::getInstance()
 *     ->getAsset('javascript', 'jquery')
 *     ->setAssetURL('/path/to/new/jquery.js');
 *
 * or, override an asset by providing a newer version.
 *
 * use \Concrete\Core\Asset\AssetList;
 * use \Concrete\Core\Asset\Asset;
 * $al = AssetList::getInstance();
 * $al->register(
 *   'javascript', 'jquery', 'path/to/new/jquery.js',
 *   array('version' => '2.0', 'position' => Asset::ASSET_POSITION_HEADER, 'minify' => false, 'combine' => false)
 *   );
 *
 * ----------------------------------------------------------------------------
 */


// On user add create new database and input the api, (then possibly populate);
 Events::addListener('on_user_add', function($event) {
    $user = $event->getUserInfoObject();
    $db = \Database::connection('wanikani_accounts');

    $username = 'wanikani_' . $user->getUserName();

    $query = 'CREATE TABLE IF NOT EXISTS ' . $username . ' (
                api_key varchar(255) NOT NULL,
                user_information_username varchar(255) NOT NULL,
                user_information_gravatar varchar(255) NOT NULL,
                user_information_level varchar(255) NOT NULL,
                requested_information_radicals_progress int(12) NOT NULL,
                requested_information_radicals_total int(12) NOT NULL,
                requested_information_kanji_progress int(12) NOT NULL,
                requested_information_kanji_total int(12) NOT NULL,
                requested_information_apprentice_radicals int(12) NOT NULL,
                requested_information_apprentice_kanji int(12) NOT NULL,
                requested_information_apprentice_vocabulary int(12) NOT NULL,
                requested_information_apprentice_total int(12) NOT NULL,
                requested_information_guru_radicals int(12) NOT NULL,
                requested_information_guru_kanji int(12) NOT NULL,
                requested_information_guru_vocabulary int(12) NOT NULL,
                requested_information_guru_total int(12) NOT NULL,
                requested_information_master_radicals int(12) NOT NULL,
                requested_information_master_kanji int(12) NOT NULL,
                requested_information_master_vocabulary int(12) NOT NULL,
                requested_information_master_total int(12) NOT NULL,
                requested_information_enlightened_radicals int(12) NOT NULL,
                requested_information_enlightened_kanji int(12) NOT NULL,
                requested_information_enlightened_vocabulary int(12) NOT NULL,
                requested_information_enlightened_total int(12) NOT NULL,
                requested_information_burned_radicals int(12) NOT NULL,
                requested_information_burned_kanji int(12) NOT NULL,
                requested_information_burned_vocabulary int(12) NOT NULL,
                requested_information_burned_total int(12) NOT NULL,
                requested_information_type varchar(255) NOT NULL,
                requested_information_character varchar(255) NOT NULL,
                requested_information_meaning varchar(255) NOT NULL,
                requested_information_onyomi varchar(255) NOT NULL,
                requested_information_kunyomi varchar(255) NOT NULL,
                requested_information_important_reading varchar(255) NOT NULL,
                requested_information_percentage varchar(255) NOT NULL,
                requested_information_kana varchar(255) NOT NULL,
                PRIMARY KEY  (api_key)
              )';

              $stmt = $db->prepare($query);
              $stmt->execute();
              // Insert the APIkey
              $db->insert($username, array('api_key' => $user->getUserEmail()));

});
