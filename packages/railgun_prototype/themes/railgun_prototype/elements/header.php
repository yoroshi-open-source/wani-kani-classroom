<?php
defined('C5_EXECUTE') or die("Access Denied.");
include('packages/railgun_prototype/header_loader.php');
$uinfo = new User(); ?>
	<header class="bg-theme1">
		<div class="container-fluid no-gutter">
		<div class="row-fluid">
			<div class="col-md-12 no-gutter">
				<?php
			$a = new GlobalArea('Header Middle Content');
			$a->display($c);
		?>
			</div>

		</div>
	</div>
	</header>
